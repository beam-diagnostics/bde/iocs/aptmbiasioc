< envVars

errlogInit(20000)

dbLoadDatabase("$(TOP_DIR)/dbd/$(APP).dbd")
$(APP)_registerRecordDeviceDriver(pdbbase)

# specify the TCP endpoint and port name
epicsEnvSet("LOCATION",                     "LAB")
epicsEnvSet("DEVICE_NAME",                  "APTMBias")
epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME):")

# configure StreamDevice path
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(DB_DIR)")


epicsEnvSet("ETHMOD_IP",            "bd-em20")
epicsEnvSet("ETHMOD_PREFIX",        "$(LOCATION):ETHMOD-020:")

# Ethernet module board ports

epicsEnvSet("I2C_TMP100_PORT",      "AK_I2C_TMP100")
epicsEnvSet("I2C_DS28CM00_PORT",    "AK_I2C_DS28CM00")
epicsEnvSet("I2C_PCF85063TP_PORT",  "AK_I2C_PCF85063TP")
epicsEnvSet("I2C_TCA9555_PORT",     "AK_I2C_TCA9555")
epicsEnvSet("I2C_LTC2991_PORT",     "AK_I2C_LTC2991")
epicsEnvSet("I2C_M24M02_PORT",      "AK_I2C_M24M02")
epicsEnvSet("I2C_IP_PORT",          "AK_I2C_COMM")


# Create the asyn port to talk to the AK-NORD server on command port 1002.
drvAsynIPPortConfigure($(I2C_IP_PORT),"$(ETHMOD_IP):1002")

###################
#   Temperature   #
###################
# AKI2CTMP100Configure(const char *portName, const char *ipPort,
#        int devCount, const char *devInfos, int priority, int stackSize);
#AKI2CTempConfigure($(I2C_TMP100_PORT), $(I2C_IP_PORT), 8, "0x48 0x49 0x4A 0x4B 0x4C 0x4D 0x4E 0x4F", 0x70, 0, 1, 0, 0)
AKI2CTMP100Configure($(I2C_TMP100_PORT), $(I2C_IP_PORT), 1, "0x49", 1, 0, 0)
dbLoadRecords("AKI2C_TMP100.db",       "P=$(ETHMOD_PREFIX),R=I2C1:Temp1:,PORT=$(I2C_TMP100_PORT),IP_PORT=$(I2C_IP_PORT),ADDR=0,TIMEOUT=1")

###################
#      EEPROM     #
###################
# AKI2CM24M02Configure(const char *portName, const char *ipPort,
#        int devCount, const char *devInfos, int priority, int stackSize);
AKI2CM24M02Configure($(I2C_M24M02_PORT), $(I2C_IP_PORT), 1, "0x50", 1, 0, 0)
dbLoadRecords("AKI2C_M24M02.db",       "P=$(ETHMOD_PREFIX),R=I2C1:Eeprom1:,PORT=$(I2C_M24M02_PORT),IP_PORT=$(I2C_IP_PORT),ADDR=0,TIMEOUT=1,NELM=262144")

###################
#  I/O Expander   #
###################
# AKI2CTCA9555Configure(const char *portName, const char *ipPort,
#        int devCount, const char *devInfos, int priority, int stackSize);
AKI2CTCA9555Configure($(I2C_TCA9555_PORT), $(I2C_IP_PORT), 1, "0x21", 1, 0, 0)
dbLoadRecords("AKI2C_TCA9555.db",        "P=$(ETHMOD_PREFIX),R=I2C1:IOExp1:,PORT=$(I2C_TCA9555_PORT),IP_PORT=$(I2C_IP_PORT),ADDR=0,TIMEOUT=1")

###################
# Voltage Monitor #
###################
# AKI2CLTC2991Configure(const char *portName, const char *ipPort,
#        int devCount, const char *devInfos, int priority, int stackSize);
#AKI2CLTC2991Configure($(I2C_LTC2991_PORT), $(I2C_IP_PORT), 1, "0x48", 0, 0, 1, 0, 0)
#dbLoadRecords("AKI2C_LTC2991.db",        "P=$(ETHMOD_PREFIX),R=I2C1:VMon1:,PORT=$(I2C_LTC2991_PORT),IP_PORT=$(I2C_IP_PORT),ADDR=0,TIMEOUT=1")

dbLoadRecords("aptmbias.db", "P=$(PREFIX),R=,ETHMOD_PREFIX=$(ETHMOD_PREFIX)")

iocInit()

dbpf("$(ETHMOD_PREFIX)I2C1:IOExp1:DirPin13", "Output")
dbpf("$(ETHMOD_PREFIX)I2C1:IOExp1:DirPin14", "Output")
dbpf("$(ETHMOD_PREFIX)I2C1:IOExp1:DirPin15", "Output")
dbpf("$(ETHMOD_PREFIX)I2C1:IOExp1:LevelPin13", 0)
dbpf("$(ETHMOD_PREFIX)I2C1:IOExp1:LevelPin14", 0)
dbpf("$(ETHMOD_PREFIX)I2C1:IOExp1:LevelPin15", 0)
